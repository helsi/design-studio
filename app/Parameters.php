<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameters extends Model
{
    protected $table = 'parameters';
    protected $fillable = ['phone_1', 'phone_2', 'admin_email', 'sale_email', 'facebook', 'telegram', 'instagram', 'address', 'lat', 'lon'];
}
