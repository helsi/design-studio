<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;

trait UploadImageTrait
{
	public function saveOriginalImage(UploadedFile $file, $path)
	{
		$originalName = $file->getClientOriginalName();
		return $file->storeAs($path, $originalName);
	}

	public function saveImage(UploadedFile $file, $path)
	{
		return $file->store($path);
	}

	public function saveImageByName(UploadedFile $file, $path, $name)
	{
		return $file->storeAs($path, $name);
	}
}
