<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['image', 'name', 'title', 'description', 'keywords', 'content'];

    public function images()
    {
        return $this->hasMany('App\ServiceImage');
    }
}
