<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ParametersRequest;
use App\Http\Controllers\Controller;
use App\Parameters;

class ParametersController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = Parameters::where('id', 1)->firstOrFail();
        return view('admin.parameters.show', ['parameters' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        /*  */
        $data = Parameters::where('id', 1)->firstOrFail();
        return view('admin.parameters.edit', ['parameters' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ParametersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(ParametersRequest $request)
    {
        $parameters = Parameters::where('id', 1)->firstOrFail();
        $parameters->fill($request->all());
        $parameters->save();
        return redirect()->route('parameters.show');
    }
}
