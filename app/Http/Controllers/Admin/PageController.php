<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;
use App\Traits\UploadImageTrait;
use App\Page;

class PageController extends Controller
{
    use UploadImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Page::all();
        return view('admin.page.index', ['pages' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $page = new Page();
        $page->fill($request->all());
        if ($file = $request->file('image')) {
            $image = $this->saveImageByName($file, 'pages', $request->slug . '.' . $file->extension());
            $page->image = $image;
        }
        $page->save();
        return redirect()->route('pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Page::where('id', $id)->firstOrFail();
        return view('admin.page.show', ['page' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Page::where('id', $id)->firstOrFail();
        return view('admin.page.edit', ['page' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PageRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {
        $page = Page::where('id', $id)->firstOrFail();
        $page->fill($request->all());
        if ($file = $request->file('image')) {
            $image = $this->saveImageByName($file, 'pages', $request->slug . '.' . $file->extension());
            $page->image = $image;
        }
        $page->save();
        return redirect()->route('pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::where('id', $id)->firstOrFail();
        $page->delete();
        return redirect()->route('pages.index');
    }
}
