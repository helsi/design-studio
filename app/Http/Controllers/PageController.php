<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    public function show($slug)
    {
    	$data = Page::where('slug', $slug)->firstOrFail();
    	return view('page', ['page' => $data]);
    }
}