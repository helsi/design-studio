<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT') {
            $slugRules = 'required|string|max:100|unique:pages,slug,' . $this->page;
        } else {
            $slugRules = 'required|string|max:100|unique:pages,slug,';
        }
        return [
            'slug' => $slugRules,
            'name' => 'required|max:100',
            'title' => 'required|max:100',
            'description' => 'max:255',
            'keywords' => 'max:255',
            'content' => 'required',
        ];
    }
}
