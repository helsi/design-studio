<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParametersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_1' => 'required|max:20',
            'phone_2' => 'max:20',
            'admin_email' => 'required|max:100',
            'sale_email' => 'max:100',
            'facebook' => 'max:100',
            'telegram' => 'max:100',
            'instagram' => 'max:100',
            'address' => 'max:100',
            'lat' => 'max:20',
            'lon' => 'max:20',
        ];
    }
}
