<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceImage extends Model
{
    protected $fillable = ['image', 'alt', 'name', 'description', 'service_id'];

    public function service()
    {
        return $this->belongsTo('App\Service');
    }
}
