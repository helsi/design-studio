<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['image', 'slug', 'name', 'title', 'description', 'keywords', 'content'];
}
