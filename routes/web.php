<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/{slug}', 'PageController@show')->name('page');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
	// Route::get('pages', 'PageController@index')->name('pages.index');

	// Route::get('pages/{id}', 'PageController@show')->name('pages.show');

	Route::resource('pages', 'PageController');
	Route::resource('services', 'ServiceController');
	Route::resource('menu', 'MenuController');
	Route::get('parameters', 'ParametersController@show')->name('parameters.show');
	Route::get('parameters/edit', 'ParametersController@edit')->name('parameters.edit');
	Route::put('parameters', 'ParametersController@update')->name('parameters.update');
	Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');
});

