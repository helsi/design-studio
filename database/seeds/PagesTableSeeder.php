<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('pages')->insert([
            [
                'slug' => 'home',
                'name' => 'Главная',
                'title' => 'Главная',
                'content' => 'Добро пожаловать!',
            ],
            [
                'slug' => 'about',
                'name' => 'О нас',
                'title' => 'О нас',
                'content' => 'Мы самые лучшие!',
            ],
            [
                'slug' => 'contacts',
                'name' => 'Контакты',
                'title' => 'Контакты',
                'content' => 'Мы в ваших сердцах!',
            ]
        ]);
	}
}
