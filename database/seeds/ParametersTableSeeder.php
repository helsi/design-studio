<?php

use Illuminate\Database\Seeder;

class ParametersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('parameters')->insert(
            [
                'phone_1' => '+380507775533',
                'admin_email' => 'admin@design-studio',
            ]
        );
    }
}
