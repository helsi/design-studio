<?php

use Illuminate\Database\Seeder;

class UsersTableSeederExample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@design-studio',
                'password' => '',
            ],
            [
                'name' => 'user 1',
                'email' => 'user1@design-studio',
                'password' => '',
            ],
            [
                'name' => 'user 2',
                'email' => 'user2@design-studio',
                'password' => '',
            ]
        ]);    
	}
}
