<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('phone_1', 20);
            $table->string('phone_2', 20)->nullable();
            $table->string('admin_email', 100);        
            $table->string('sale_email', 100)->nullable();
            $table->string('facebook', 100)->nullable();
            $table->string('telegram', 100)->nullable();
            $table->string('instagram', 100)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('lat', 20)->nullable();
            $table->string('lon', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameters');
    }
}
