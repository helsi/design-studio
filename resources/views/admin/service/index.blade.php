@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active">Services</li>
@endsection

@section('title')
Services
@endsection

@section('content')
<ul>
	@foreach($services as $service)
	<li>
		<a href="{{ route('services.show', ['id' => $service->id]) }}">{{ $service->name }}</a>
	</li>
	@endforeach	
</ul>
{{ $services->links() }}
<div><a href="{{ route('services.create') }}">Create</a></div>
@endsection
