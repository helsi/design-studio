@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('services.index') }}">Services</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('title')
Create Service
@endsection

@section('content')
{!! Form::open(['url' => route('services.store'), 'method' => 'post', 'files' => true]) !!}
    @include('admin.service.form')
{!! Form::close() !!}
<div><a href="{{ route('services.index') }}">List</a></div>
@endsection
