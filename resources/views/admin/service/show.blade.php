@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('services.index') }}">Services</a></li>
    <li class="breadcrumb-item active">{{ $service->name }}</li>
@endsection

@section('title')
{{ $service->name }}
@endsection

@section('content')
<table>
	<tr>
		<th>Image</th>
		<td><img src="{{ asset($service->image) }}" alt="{{ $service->name }}"></td>
	</tr>
	<tr>
		<th>Slug</th>
		<td>{{ $service->slug }}</td>
	</tr>
	<tr>
		<th>Name</th>
		<td>{{ $service->name }}</td>
	</tr>
	<tr>
		<th>Title</th>
		<td>{{ $service->title }}</td>
	</tr>
	<tr>
		<th>Description</th>
		<td>{{ $service->description }}</td>
	</tr>
	<tr>
		<th>Keywords</th>
		<td>{{ $service->keywords }}</td>
	</tr>
	<tr>
		<th>Content</th>
		<td>{{ $service->content }}</td>
	</tr>
</table>
<div><a href="{{ route('services.index') }}">List</a></div>
<div><a href="{{ route('services.edit', ['id' => $service->id]) }}">edit</a></div>
<div>{!! Form::open(['url' => route('services.destroy', ['id' => $service->id]), 'method' => 'delete']) !!}
	{{ Form::button('<i class="fas fa-times"></i> Delete', ['type' => 'submit']) }}
{!! Form::close() !!}</div>
@endsection
