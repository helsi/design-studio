@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('services.index') }}">Services</a></li>
    <li class="breadcrumb-item"><a href="{{ route('services.show', ['id' => $service->id]) }}">{{ $service->name }}</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('title')
Edit {{ $service->name }}
@endsection

@section('content')
{!! Form::open(['url' => route('services.update', ['id' => $service->id]), 'method' => 'put', 'files' => true]) !!}
    @include('admin.service.form', ['service' => $service])
{!! Form::close() !!}
<div><a href="{{ route('services.index') }}">List</a></div>
<div><a href="{{ route('services.show', ['id' => $service->id]) }}">show</a></div>
@endsection
