<div class="card card-default">
  <form role="form">
    <div class="card-body">
      <div class="form-group">
		{{ Form::label('name', 'Name') }}
        {{ Form::text('name', isset($service) ? $service->name : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', isset($service) ? $service->title : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', isset($service) ? $service->description : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('keywords', 'Keywords') }}
        {{ Form::textarea('keywords', isset($service) ? $service->keywords : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('content', 'Content') }}
        {{ Form::textarea('content', isset($service) ? $service->content : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('image', 'Image') }}
        {{ Form::file('image', ['class' => 'form-control']) }}
      </div>
    </div>
    <div class="card-footer">
    	{{ Form::button('<i class="fas fa-check"></i> Send', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
    </div>
  </form>
</div>
