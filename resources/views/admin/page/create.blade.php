@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('pages.index') }}">Pages</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('title')
Create Page
@endsection

@section('content')
{!! Form::open(['url' => route('pages.store'), 'method' => 'post', 'files' => true]) !!}
    @include('admin.page.form')
{!! Form::close() !!}
<div><a href="{{ route('pages.index') }}">List</a></div>
@endsection
