@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('pages.index') }}">Pages</a></li>
    <li class="breadcrumb-item"><a href="{{ route('pages.show', ['id' => $page->id]) }}">{{ $page->name }}</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('title')
Edit {{ $page->name }}
@endsection

@section('content')
{!! Form::open(['url' => route('pages.update', ['id' => $page->id]), 'method' => 'put', 'files' => true]) !!}
    @include('admin.page.form', ['page' => $page])
{!! Form::close() !!}
<div><a href="{{ route('pages.index') }}">List</a></div>
<div><a href="{{ route('pages.show', ['id' => $page->id]) }}">show</a></div>
@endsection
