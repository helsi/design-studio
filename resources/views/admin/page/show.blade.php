@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('pages.index') }}">Pages</a></li>
    <li class="breadcrumb-item active">{{ $page->name }}</li>
@endsection

@section('title')
{{ $page->name }}
@endsection

@section('content')
<div class="card">
  <div class="card-body p-0">
    <table class="table table-condensed">
      <thead>
        <tr>
          <th>Name</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Slug</td>
          <td>{{ $page->slug }}</td>
        </tr>
        <tr>
          <td>Name</td>
          <td>{{ $page->name }}</td>
        </tr>
        <tr>
          <td>Title</td>
          <td>{{ $page->title }}</td>
        </tr>
        <tr>
          <td>Description</td>
          <td>{{ $page->description }}</td>
        </tr>
        <tr>
          <td>Keywords</td>
          <td>{{ $page->keywords }}</td>
        </tr>
        <tr>
          <td>Content</td>
          <td>{{ $page->content }}</td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
</div>

<div><a href="{{ route('pages.index') }}">List</a></div>
<div><a href="{{ route('pages.edit', ['id' => $page->id]) }}">edit</a></div>
<div>{!! Form::open(['url' => route('pages.destroy', ['id' => $page->id]), 'method' => 'delete']) !!}
	{{ Form::button('<i class="fas fa-times"></i> Delete', ['type' => 'submit']) }}
{!! Form::close() !!}</div>
@endsection
