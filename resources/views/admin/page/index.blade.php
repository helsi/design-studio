@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active">Pages</li>
@endsection

@section('title')
Pages
@endsection

@section('content')
<div class="card">
  <div class="card-body p-0">
    <table class="table table-condensed">
      <thead>
        <tr>
          <th style="width: 10px">#</th>
          <th>Name</th>
          <th>Operations</th>
        </tr>
      </thead>
      <tbody>
		@foreach($pages as $page)
        <tr>
          <td>{{ $page->id }}</td>
          <td><a href="{{ route('pages.show', ['id' => $page->id]) }}">{{ $page->name }}</a></td>
          <td></td>
        </tr>
		@endforeach	
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
</div>
<div><a href="{{ route('pages.create') }}">Create</a></div>
@endsection
