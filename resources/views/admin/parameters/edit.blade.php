@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('parameters.show') }}">Parameters</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('title')
Edit Parameters
@endsection

@section('content')
{!! Form::open(['url' => route('parameters.update'), 'method' => 'put']) !!}
    @include('admin.parameters.form', ['parameters' => $parameters])
{!! Form::close() !!}
<div><a href="{{ route('parameters.show') }}">show</a></div>
@endsection
