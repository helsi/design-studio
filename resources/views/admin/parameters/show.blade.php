@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active">Parameters</li>
@endsection

@section('title')
Parameters
@endsection

@section('content')
<div class="card">
  <div class="card-body p-0">
    <table class="table table-condensed">
      <thead>
        <tr>
          <th>Name</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Phone 1</td>
          <td>{{ $parameters->phone_1 }}</td>
        </tr>
        <tr>
          <td>Phone 2</td>
          <td>{{ $parameters->phone_2 }}</td>
        </tr>
        <tr>
          <td>Admin email</td>
          <td>{{ $parameters->admin_email }}</td>
        </tr>
        <tr>
          <td>Sale email</td>
          <td>{{ $parameters->sale_email }}</td>
        </tr>
<!--                     <tr>
          <td>Phone 1</td>
          <td>{{ $parameters->phone_1 }}</td>
        </tr>
        <tr>
          <td>Phone 1</td>
          <td>{{ $parameters->phone_1 }}</td>
        </tr>
        <tr>
          <td>Phone 1</td>
          <td>{{ $parameters->phone_1 }}</td>
        </tr> -->
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
</div>
<div><a href="{{ route('parameters.edit') }}">edit</a></div>
@endsection
