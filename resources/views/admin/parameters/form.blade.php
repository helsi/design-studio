<div class="card card-default">
  <form role="form">
    <div class="card-body">
      <div class="form-group">
        {{ Form::label('phone_1', 'Phone 1') }}
        {{ Form::text('phone_1', isset($parameters) ? $parameters->phone_1 : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('phone_2', 'Phone 2') }}
        {{ Form::text('phone_2', isset($parameters) ? $parameters->phone_2 : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
		{{ Form::label('admin_email', 'admin email') }}
        {{ Form::text('admin_email', isset($parameters) ? $parameters->admin_email : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
		{{ Form::label('sale_email', 'admin email') }}
        {{ Form::text('sale_email', isset($parameters) ? $parameters->sale_email : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('facebook', 'facebook') }}
        {{ Form::text('facebook', isset($parameters) ? $parameters->facebook : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('telegram', 'telegram') }}
        {{ Form::text('telegram', isset($parameters) ? $parameters->telegram : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('instagram', 'instagram') }}
        {{ Form::text('instagram', isset($parameters) ? $parameters->instagram : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('address', 'address') }}
        {{ Form::textarea('address', isset($parameters) ? $parameters->address : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('lat', 'lat') }}
        {{ Form::text('lat', isset($parameters) ? $parameters->lat : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
        {{ Form::label('lon', 'lon') }}
        {{ Form::text('lon', isset($parameters) ? $parameters->lon : '', ['class' => 'form-control']) }}
      </div>
    </div>
    <div class="card-footer">
    	{{ Form::button('<i class="fas fa-check"></i> Send', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
    </div>
  </form>
</div>
