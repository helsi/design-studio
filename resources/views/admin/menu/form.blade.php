<div class="card card-default">
  <form role="form">
    <div class="card-body">
      <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', isset($menu) ? $menu->name : '', ['class' => 'form-control']) }}
      </div>
      <div class="form-group">
		{{ Form::label('href', 'Href') }}
        {{ Form::text('href', isset($menu) ? $menu->href : '', ['class' => 'form-control']) }}
      </div>
    </div>
    <div class="card-footer">
    	{{ Form::button('<i class="fas fa-check"></i> Send', ['type' => 'submit', 'class' => 'btn btn-primary']) }}
    </div>
  </form>
</div>
