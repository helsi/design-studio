@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('menu.index') }}">Menu</a></li>
    <li class="breadcrumb-item"><a href="{{ route('menu.show', ['id' => $menu->id]) }}">{{ $menu->name }}</a></li>
    <li class="breadcrumb-item active">Edit</li>
@endsection

@section('title')
Edit {{ $menu->name }}
@endsection

@section('content')
{!! Form::open(['url' => route('menu.update', ['id' => $menu->id]), 'method' => 'put']) !!}
    @include('admin.menu.form', ['menu' => $menu])
{!! Form::close() !!}
<div><a href="{{ route('menu.index') }}">List</a></div>
<div><a href="{{ route('menu.show', ['id' => $menu->id]) }}">show</a></div>
@endsection
