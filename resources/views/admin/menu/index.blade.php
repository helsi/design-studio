@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active">Menu</li>
@endsection

@section('title')
Menu
@endsection

@section('content')
<ul>
	@foreach($menu as $menuItem)
	<li>
		<a href="{{ route('menu.show', ['id' => $menuItem->id]) }}">{{ $menuItem->name }}</a>
	</li>
	@endforeach	
</ul>
<div><a href="{{ route('menu.create') }}">Create</a></div>
@endsection

