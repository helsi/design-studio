@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('menu.index') }}">Menu</a></li>
    <li class="breadcrumb-item active">Create</li>
@endsection

@section('title')
Create Menu
@endsection

@section('content')
{!! Form::open(['url' => route('menu.store'), 'method' => 'post']) !!}
    @include('admin.menu.form')
{!! Form::close() !!}
<div><a href="{{ route('menu.index') }}">List</a></div>
@endsection
