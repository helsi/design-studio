@extends('admin.layout')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('menu.index') }}">Menu</a></li>
    <li class="breadcrumb-item active">{{ $menu->name }}</li>
@endsection

@section('title')
{{ $menu->name }}
@endsection

@section('content')
<table>
	<tr>
		<th>Name</th>
		<td>{{ $menu->name }}</td>
	</tr>
	<tr>
		<th>Href</th>
		<td>{{ $menu->href }}</td>
	</tr>
</table>
<div><a href="{{ route('menu.index') }}">List</a></div>
<div><a href="{{ route('menu.edit', ['id' => $menu->id]) }}">edit</a></div>
<div>{!! Form::open(['url' => route('menu.destroy', ['id' => $menu->id]), 'method' => 'delete']) !!}
	{{ Form::button('<i class="fas fa-times"></i> Delete', ['type' => 'submit']) }}
{!! Form::close() !!}</div>
@endsection
